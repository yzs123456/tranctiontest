create table user
(
    id        bigint auto_increment comment '用户ID'
        primary key,
    user_name varchar(50) null comment '用户名称',
    phone     varchar(50) null comment '手机号码',
    password  varchar(50) null comment '密码',
    remark    varchar(50) null comment '备注'
)
    comment '用户信息表';

# 2022/09/07
alter table user add column birthday datetime null comment '生日';
