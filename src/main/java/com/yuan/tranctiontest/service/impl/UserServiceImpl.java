package com.yuan.tranctiontest.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.tranctiontest.aop.Before;
import com.yuan.tranctiontest.entity.UserEntity;
import com.yuan.tranctiontest.exception.RollBackException;
import com.yuan.tranctiontest.mapper.UserMapper;
import com.yuan.tranctiontest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author yuanzhisong
 * @since 2022-08-17
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {


    @Autowired
    private UserMapper userMapper;

//    @Autowired
//    private UserService userService;


    // 放在service未加注解:没有事务功能,所以代码依旧直接执行
    @Override
//    @Transactional(rollbackFor = ArithmeticException.class)
//    @Transactional(rollbackFor = Exception.class)
//    @Transactional(rollbackFor = RollBackException.class)
    public Object test6() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 直接加注解事务生效 并没有指明什么事务 空指针和算术异常都会出现事务回滚 成功!
    @Override
    @Transactional
    public Object test7() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
//        System.out.println(9 / 0);
        List<Integer> list = null;
        System.out.println(list.size());
        return "测试";
    }


    // 因为RollBackException没有继承运行时异常 所以事务并没有进行回滚!!!
    @Override
    @Transactional
    public Object test8() throws RollBackException {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        if (true) {
            throw new RollBackException("出现异常");
        }
        return "测试";
    }


    // 抛出运行时异常 事务进行回滚
    @Override
    @Transactional
    public Object test9() throws RollBackException {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        if (true) {
            throw new RuntimeException("出现异常");
        }
        return "测试";
    }


    // 增加了RollBackException异常 其他运行时异常依旧会滚
    @Override
    @Transactional(rollbackFor = RollBackException.class)
    public Object test10() throws RollBackException {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        if (true) {
            throw new RuntimeException("出现异常");
        }
        return "测试";
    }


    // 增加了RollBackException异常 抛出RollBackException异常 事务会回滚!!!
    @Override
    @Transactional(rollbackFor = RollBackException.class)
    public Object test11() throws RollBackException {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        if (true) {
            throw new RollBackException("出现异常");
        }
        return "测试";
    }


    // 用了@Transactional()注解 用private修饰符会直接报错 因为不在同一个类里面
//    @Override
//    @Transactional()
//    private Object test12() throws RollBackException {
//        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
//        user.setRemark(String.valueOf(new Random().nextInt()));
//        userMapper.updateById(user);
//        System.out.println(9 / 0);
//        return "测试";
//    }


    // 13+14:两个方法都用到了@Transactional(),且第一个方法为public,第二个方法为缺省,抛出运行时异常,事务进行回滚,
    // 应该是被大的事务给包进去了 里面的事务有没有被捕获到其实已经无所谓了!!! 反正外面有大哥存在
    @Override
    @Transactional
    public Object test13() {
        return test14();
    }

    @Transactional
    Object test14() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 15+16:两个方法都用到了@Transactional(),且第一个方法为public,第二个方法为protected,抛出运行时异常,事务进行回滚,
    // 同理应该是被大的事务给包进去了 里面的事务有没有被捕获到其实已经无所谓了!!! 反正外面有大哥存在
    @Override
    @Transactional
    public Object test15() {
        return test16();
    }

    @Transactional
    protected Object test16() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 17+18:两个方法都用到了@Transactional(),且两个方法都为public,抛出运行时异常,事务进行回滚
    @Override
    @Transactional
    public Object test17() {
        return test18();
    }

    @Transactional
    public Object test18() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 19+20:@Transactional方法调用非@Transactional方法时,事务会回滚,被大的事务给包进去了
    @Override
    @Transactional
    public Object test19() {
        return test20();
    }

    public Object test20() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 21+22:两个方法都没有加@Transactional注解 没有事务 所以也不会存在回滚的问题 方法执行
    @Override
    public Object test21() {
        return test22();
    }

    public Object test22() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 事务失效的特例! 非事务方法调用事务方法 无论被调用的方法修饰符是谁 事务不会回滚!!!
    @Override
    public Object test23() {
        return test24();
    }


    @Transactional
    public Object test24() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 捕获异常但是不抛出事务不会回滚! 捕获异常抛出运行时异常事务会进行回滚
    @Transactional
    public Object test25() {

        UserEntity user = null;
        try {
            user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
            user.setRemark(String.valueOf(new Random().nextInt()));
            userMapper.updateById(user);
            System.out.println(9 / 0);
            return "测试";
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("出现异常");
        }
    }


    // 在事务方法内开启一个新的线程且在新的线程内发生异常!!!(在默认线程内有异常事务会回滚!!!) 事务不会回滚
    // 因为spring实现事务的原理是通过ThreadLocal把数据库连接绑定到当前线程中，新开启一个线程获取到的连接就不是同一个了。
    @Transactional
    public Object test26() {

        UserEntity user1 = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user1.setRemark(String.valueOf("4"));
        userMapper.updateById(user1);

        new Thread(() -> {
            System.out.println(9 / 0);
            UserEntity user2 = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
            user2.setRemark(String.valueOf("5"));
            userMapper.updateById(user2);
        }).start();
        return "测试";
    }

//    @Override
//    public Object testBy() {
//        userService.select();
//        return null;
//    }
//
//    @Override
//    public Object select() {
//        return userMapper.selectList(null);
//    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object testAfter() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        return "success";
    }
}
