package com.yuan.tranctiontest.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.tranctiontest.entity.UserEntity;
import com.yuan.tranctiontest.exception.RollBackException;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author yuanzhisong
 * @since 2022-08-17
 */
public interface UserService extends IService<UserEntity> {

    Object test6();

    Object test7();

    Object test8() throws RollBackException;

    Object test9() throws RollBackException;

    Object test10() throws RollBackException;

    Object test11() throws RollBackException;

//    Object test12();

    Object test13();

    Object test15();

    Object test17();

    Object test19();

    Object test21();

    Object test23();

    Object test25();

    Object test26();

//    Object testBy();
//
//    Object select();

    Object testAfter();
}
