package com.yuan.tranctiontest.exception;

/**
 * 回滚异常
 */
public class RollBackException extends Exception {

    /**
     * 错误信息
     */
    private String message;

    /**
     * 回滚异常
     * @param msg 错误信息
     */
    public RollBackException(String msg) {
        super(msg);
        this.message = msg;
    }

    /**
     * 获取异常信息
     * @return 异常信息
     */
    @Override
    public String getMessage() {
        return message;
    }


    /**
     * 设置异常信息
     * @param message 异常信息
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
