package com.yuan.tranctiontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TranctiontestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TranctiontestApplication.class, args);
    }
}
