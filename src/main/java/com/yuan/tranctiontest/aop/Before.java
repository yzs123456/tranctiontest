package com.yuan.tranctiontest.aop;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Before {

    /**
     * 自定义内容
     */
    String message() default "";
}
