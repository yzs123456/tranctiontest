package com.yuan.tranctiontest.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @Date 2023/1/4
 */
@Aspect
@Component
public class BeforeAspect {

    // 配置织入点
    @Pointcut("@annotation(com.yuan.tranctiontest.aop.Before)")
    public void logPointCut() {
    }


    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @Before("logPointCut()")
    public void doAfterReturning(JoinPoint joinPoint){
        int i = 0;
        System.out.println("你好");
        System.out.println(1 / 0);
    }
}
