package com.yuan.tranctiontest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.tranctiontest.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author yuanzhisong
 * @since 2022-08-17
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

}
