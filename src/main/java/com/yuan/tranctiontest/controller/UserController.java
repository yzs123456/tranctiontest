package com.yuan.tranctiontest.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yuan.tranctiontest.aop.Before;
import com.yuan.tranctiontest.dto.Test;
import com.yuan.tranctiontest.entity.UserEntity;
import com.yuan.tranctiontest.exception.RollBackException;
import com.yuan.tranctiontest.mapper.UserMapper;
import com.yuan.tranctiontest.service.UserService;
import org.aspectj.lang.annotation.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Random;

/**
 * 测试事务
 *
 * @author yuanzhisong
 * @since 2022-08-17
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;


    /**
     * 测试模型和视图
     */
    @GetMapping("/testMAV")
    public Object testMAV() {
        System.out.println("testMAV");
        return "testMAV";
    }

    /**
     * 用private方法进行测试
     */
    @GetMapping("/test")
    @Transactional
    public Object test() throws RollBackException {
        return test2();
//        return userService.test26();
    }

//     @Transactional用在private修饰符的方法idea直接报错
//    @Transactional
//    private Object test1() {
//        return userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
//    }

    // @Transactional用在default修饰符的方法上注解会失效
    @Transactional
    Object test2() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }

    // @Transactional用在protected修饰符的方法上注解会失效
    @Transactional
    protected Object test3() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }

    // @Transactional用在public修饰符的方法上注解会失效
    // 在类上或者接口上加上注解就可以解决上述问题
    @Transactional()
    public Object test4() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }

    // @Transactional用在public修饰符的方法上注解会失效
    // 在类上或者接口上加上注解就可以解决上述问题
    @Transactional(rollbackFor = ArithmeticException.class)
    public Object test5() {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setRemark(String.valueOf(new Random().nextInt()));
        userMapper.updateById(user);
        System.out.println(9 / 0);
        return "测试";
    }


    // 测试注解: @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//    @PostMapping("/testZj")
//    public Object testZj(@RequestBody Test test) {
//        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
//        user.setBirthday(test.getBirthday());
//        userMapper.updateById(user);
//        return user;
//    }

    @GetMapping("/testZj")
    public Object testZj(@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam("birthday") Date birthday) {
        UserEntity user = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("id", 1));
        user.setBirthday(birthday);
        userMapper.updateById(user);
        return user;
    }


    /**
     * 测试spring是否能够注入自己
     *
     * :当有循环依赖的时候,项目启动直接报错!!!
     */
//    @GetMapping("/testBy")
//    public Object testBy() {
//        return userService.testBy();
//    }


    /**
     * 测试spring后置处理器是否能导致回滚
     *
     * 结论:后置处理器发生异常无法进行回滚 前置处理器因为发生异常无法执行核心代码所以无需回滚!!! 需要注意后置处理器存在的问题!!!
     */
    @Before
    @GetMapping("/testAfter")
    public Object testAfter() {
        return userService.testAfter();
    }
}
